package net.stuntguy3000;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class SimpleNear extends JavaPlugin {
	
	public String Prefix = "&8[&7SimpleNear&8] &7";
	
	public void onEnable() {
		getCommand("nearby").setExecutor(this);
		
		getConfig().options().copyDefaults(true);
		saveConfig();
	}
	
	public String c(String input)
	{
		return ChatColor.translateAlternateColorCodes('&', input);
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if (sender instanceof Player)
		{
			Player p = (Player) sender;
			
			if (p.hasPermission("SimpleNear.use"))
			{
				List<String> distance = new ArrayList<String>();
				for (Player target : Bukkit.getOnlinePlayers())
				{
					if (!target.getName().equalsIgnoreCase(p.getName()) && p.getWorld().getName().equalsIgnoreCase(target.getWorld().getName()))
					{
						if ((int) target.getLocation().distance(p.getLocation()) <= getConfig().getInt("Distance"))
						{	
							if (target.hasPermission("SimpleNear.hidden"))
							{
								if (p.hasPermission("SimpleNear.hidden.bypass"))
								{
									distance.add("&7[H] " + target.getName() + " &6(&e" + (int) target.getLocation().distance(p.getLocation()) + "&6)");
								}
							} else {
								distance.add("&e" + target.getName() + " &6(&e" + (int) target.getLocation().distance(p.getLocation()) + "&6)");
							}
						}
					}
				}
				
				if (distance.size() == 0)
				{
					p.sendMessage(c(Prefix + "&eNobody is within &6" + getConfig().getInt("Distance") + "&e blocks of you..."));
				} else {
					String string = "";
					int Count = 0;
					
					for (String d2 : distance)
					{
						Count ++;
						
						if (Count == distance.size())
						{
							string = string + d2;
						} else {
							string = string + d2 + "&8, ";
						}
					}
					
					p.sendMessage(c(""));
					p.sendMessage(c(Prefix + "&bNearby Players:"));
					p.sendMessage(c(Prefix + string));
				}
				
				return true;
			} else {
				p.sendMessage(c(Prefix + "&cSorry, you cannot use this command!"));
				
				return true;
			}
		} else {
			sender.sendMessage("You know you are not a player, right?");
			return true;
		}
	}
}
